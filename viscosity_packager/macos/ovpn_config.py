import shutil
from pathlib import Path
from typing import Any

import click

from .._system.ovpn_config import ViscosityOVPNConfig


@click.command()
@click.pass_obj
def cli(obj: dict[str, Any]) -> None:
    """Converts OpenVPN Configs to Viscosity Format"""
    env = obj['ENV']
    config = obj['CONFIG']
    vpn_config_path = Path(config['ovpn_configs_folder']).expanduser().absolute()

    for entry in config['vpn_configs']:
        src_path = vpn_config_path / entry.pop('src')
        dst_path = env / config['viscosity_configs_folder'] / entry.pop('dst')
        if dst_path.exists():
            shutil.rmtree(dst_path)
        dst_path.mkdir(parents=True)

        # create viscosity ovpn config object
        viscosity_config = ViscosityOVPNConfig(entry.pop('name'))

        # load ovpn config file
        viscosity_config.load_ovpn_config(src_path)

        # set custom viscosity settings
        for setting, value in entry.items():
            setattr(viscosity_config, setting, value)

        viscosity_config.create_config_files(dst_path)
