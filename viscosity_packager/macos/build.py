import json
import plistlib
import subprocess
from pathlib import Path
from pprint import pformat
from typing import Any

import click


@click.command()
@click.argument('project', type=click.Path(exists=True, dir_okay=False, path_type=Path))
@click.option(
    '-b',
    '--build-dir',
    type=click.Path(file_okay=False, writable=False, path_type=Path),
    help='build directory',
    default='build',
    show_default=True,
)
@click.option('-s', '--sign', is_flag=True, help='Sign installer package')
@click.option('-n', '--notarize', is_flag=True, help='Sign and notarize installer package')
@click.option(
    '-c',
    '--certificate',
    type=click.STRING,
    help='Certificate Name',
    default=lambda: click.get_current_context().obj['CONFIG']['apple_developer']['signing_certificate'],
    show_default='Config Value',
)
@click.option(
    '-p',
    '--keychain-profile',
    'keychain_profile',
    type=click.STRING,
    help='App Store Connect API Keychain profile name (only for notarization required)',
)
@click.pass_obj
def cli(  # noqa PLR0913
    obj: dict[str, Any],
    project: Path,
    build_dir: Path,
    sign: bool,
    notarize: bool,
    certificate: str,
    keychain_profile: str,
) -> None:
    """Build package"""
    env = obj['ENV']

    if notarize and not sign:
        click.secho('Cannot notarize installer package without signing installer package', fg='red')
        raise click.Abort

    project_path = project.expanduser().absolute()
    with open(project_path, 'rb') as infile:
        project_plist = plistlib.load(infile)
    package_name = project_plist['PROJECT']['PROJECT_SETTINGS']['NAME']
    package_filename = f'{package_name}.pkg'

    build_path = build_dir.expanduser().absolute()
    package_path = build_path / package_filename

    build_command = ['packagesbuild', '--build-folder', str(build_path)]
    if sign:
        build_command.extend(['--identity', certificate])
    build_command.append(str(project_path))
    click.echo(f'Running build{" and signing" if sign or notarize else ""}.')
    if sign:
        click.echo('You may be asked for access to the certificate multiple times.')
    try:
        subprocess.run(build_command, cwd=env, capture_output=True, check=True)  # noqa S603
        click.secho('Build was successful.', fg='green')
    except subprocess.CalledProcessError as e:
        click.secho(
            f"""Build failed.
Command executed: {" ".join(build_command)}
Command stdout output:\n{e.stdout.decode("utf-8")}
Command output:\n{e.stderr.decode("utf-8")}""",
            fg='red',
        )
        raise click.Abort from e

    if notarize:
        click.echo('Submiting package for notarization')
        if keychain_profile is None:
            keychain_profile = click.prompt('App Store Connect API Keychain profile name')
        notarize_command = [
            'xcrun',
            'notarytool',
            'submit',
            '--keychain-profile',
            str(keychain_profile),
            '--output-format',
            'json',
            '--wait',
            str(package_path),
        ]
        try:
            notarization_run = subprocess.run(notarize_command, cwd=env, capture_output=True, check=True)  # noqa S603
            notarization_result = json.loads(notarization_run.stdout.decode('utf-8'))
            if notarization_result['status'] == 'Accepted':
                click.secho('Notarization successful.', fg='green')
                click.secho(pformat(notarization_result), fg='blue')
            else:
                click.secho('Notarization failed.', fg='red')
                click.secho(pformat(notarization_result), fg='blue')
        except subprocess.CalledProcessError as e:
            click.secho(
                f"""Upload for notarization failed.
Command executed: {" ".join(notarize_command)}
Command stdout output:\n{e.stdout.decode("utf-8")}
Command stderr output:\n{e.stderr.decode("utf-8")}
""",
                fg='red',
            )
            raise click.Abort from e
