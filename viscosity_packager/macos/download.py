import plistlib
import shutil
import tempfile
import urllib.parse
from pathlib import Path
from typing import Any

import click
import dmglib
import requests
from defusedxml import ElementTree


@click.command(short_help='download Viscosity release')
@click.pass_obj
def cli(obj: dict[str, Any]) -> None:
    """Downloads current Viscosity Release and Stores it in Build Environment Folder"""
    env = obj['ENV']

    local_version = None
    app = 'Viscosity.app'
    dest = env / 'Viscosity' / app
    if dest.exists():
        with open(dest / 'Contents' / 'Info.plist', 'rb') as plistfile:
            plist = plistlib.load(plistfile)
        local_version = int(plist['CFBundleVersion'])

    res = requests.get('https://swupdate.sparklabs.com/appcast/mac/release/viscosity/', timeout=15)
    res.raise_for_status()
    xml = ElementTree.fromstring(res.content)
    encl = xml.find('channel/item/enclosure')
    download_url = encl.attrib['url']
    appcast_version = int(encl.attrib['{http://www.andymatuschak.org/xml-namespaces/sparkle}version'])

    if local_version == appcast_version:
        click.confirm(
            f'Local local_version ({local_version}) is identic with downloadable ({appcast_version}). Continue anyway?',
            abort=True,
        )
    else:
        click.secho(f'New version available ({appcast_version} > {local_version})', fg='green')

    download_filename = Path(urllib.parse.urlparse(download_url).path).name

    with tempfile.TemporaryDirectory() as tmpdir:
        click.echo(f'Downloading {download_filename} from {download_url}...')
        res = requests.get(download_url, timeout=180)

        image_path = Path(tmpdir) / download_filename
        with open(image_path, 'wb') as outfile:
            outfile.write(res.content)

        if not dmglib.dmg_is_valid(str(image_path)):
            click.secho('Downloaded image not valid.', fg='red', bold=True)
            raise click.Abort

        click.echo('Mounting image...')
        dmg = dmglib.DiskImage(image_path)
        mount_point = Path(dmg.attach()[0])
        src = mount_point / app

        click.echo(f'Copy {src} from image to {dest}.')
        if dest.exists():
            shutil.rmtree(dest)
        shutil.copytree(src, dest, symlinks=True)

        click.echo('Unmounting image...')
        dmg.detach()
