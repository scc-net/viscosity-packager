from pathlib import Path

import click
import yaml
from click import Context

from .. import DynamicMultiCommandFactory

DynamicMultiCommand = DynamicMultiCommandFactory().create(__file__, __package__)


@click.command(cls=DynamicMultiCommand)
@click.option(
    '-b',
    '--build-path',
    'build_path',
    type=click.Path(exists=True, file_okay=False, path_type=Path),
    default='.',
    help='Folder containing all Viscosity Build directories',
)
@click.option(
    '-c',
    '--config',
    'config_path',
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
    default='config.yml',
    help='Configuration file in YAML format',
)
@click.pass_context
def cli(ctx: Context, build_path: Path, config_path: Path) -> None:
    """MacOS Build and Config commands"""
    env = build_path.expanduser().absolute()
    config_path = config_path.expanduser().absolute()

    with open(config_path, 'r') as infile:
        config = yaml.safe_load(infile.read())

    ctx.obj = {'ENV': env, 'CONFIG': config}
