import click

from ._utils.core import DynamicMultiCommandFactory

CONTEXT_SETTINGS = {'help_option_names': ['-h', '--help'], 'max_content_width': 120}
DynamicMultiCommand = DynamicMultiCommandFactory().create(__file__, __package__)


@click.command(cls=DynamicMultiCommand, context_settings=CONTEXT_SETTINGS)
def cli() -> None:
    """Helper Tool to configure and build viscosity installer for macOS (and maybe Windows in future)."""
