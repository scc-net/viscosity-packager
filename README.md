# viscosity-packager

This cli tools with packaging Viscosity (yet only on MacOS, Windows support may be implemented in the future).

## Development
Dependencies are managed with [Poetry](https://python-poetry.org).

Code quality is checked by `flake8` and `pylint`, so before commiting please check with
````shell
poetry run flake8 .
````
and
```shell
poetry run pylint viscosity_packager
```

## Installation
Latest release from [Gitlab Package Registry](https://git.scc.kit.edu/scc-net/viscosity-packager/-/packages):
```
 pip install viscosity-packager --extra-index-url https://git.scc.kit.edu/api/v4/projects/23307/packages/pypi/simple
 ```

From source:
```
pip install git+git@git.scc.kit.edu:scc-net/viscosity-packager.git
```

## Further requirements
### MacOS
XCode is required. You can install it from the Apple App Store. The Command Line Tools are also required, you can install them with
```shell
xcode-select --install
```

For packaging the tool [Packages](http://s.sudre.free.fr/Software/Packages/about.html) is required. It is available in the Homebrew Cask and can be installed with
````shell
brew install --cask packages
````

#### NEW METHOD: xcrun notarytool
Use an app store connect team api key with developer role.
```shell
xcrun notarytool store-credentials --key <keyfile> --key-id <keyid> --issuer <issuer> <profile-name>
```
Add `--sync` to store in iCloud Keychain.

## Usage
The CLI is build with [click](https://click.palletsprojects.com). For each command help is available with `-h`. Example
```
$ viscosity-packager -h
Usage: viscosity-packager [OPTIONS] COMMAND [ARGS]...

  Helper Tool to configure and build viscosity installer for MacOS (and maybe Windows in future).

Options:
  -h, --help  Show this message and exit.

Commands:
  macos  MacOS Build and Config commands
```
### Configuration
This tool needs an configuration file for building the Viscosity Installer. An example can be found in `config.example.yml`.
By default this tool searchs inside the Viscosity Installer-Folder for `config.yml`.

Inside the config for each connection (`vpn_configs`) the custom config options for Viscosity can be redefined (eg. `ipv6: False` or `usepeerdns: True`)
### MacOS
You'll have to prepare an directory following the [Viscosity KB](https://www.sparklabs.com/support/kb/article/bundling-viscosity-with-vpn-connections-preferences-mac).
If you don't want to pipe all configs through Viscosity, this tool has an configurable converter for OpenVPN Inline Configs.

To download the most recent `Viscosity.app` just run
```
viscosity-packager macos download
```

The relevant config keys are `ovpn_configs_folder` for the folder containing all source `*.ovpn` files, `viscosity_configs_folder`, the Folder in which the converted configs should be placed, and `vpn_configs`, a list of dicts containing config for each file which should get converted.
Example command for conversion:
```shell
viscosity-packager macos ovpn-config
```

Building, signing and notarization can be done with
```shell
viscosity-packager macos build PROJECTFILE.pkgproj --notarize
```
You can optionally pass username (`-u` / `--username`) and keychain password entry (`-p` / `--password`) directly or you will get prompted to enter them when needed.
